#!/bin/bash
#########################
#   名称: Install
#   用途: 负责主要的工作
#########################

#shellcheck disable=SC1090
#shellcheck disable=SC2143

#此时_this_ThrowErrorAndExit还没初始化，因此只返回2并退出
readonly HERE="$(cd "$(dirname "${BASH_SOURCE:-$0}")" || exit 2;pwd)";
readonly RESOURCES="$HERE/resources";
readonly USERNAME="$(whoami)";

source "$HERE"/modules/Statics
source "$HERE"/modules/Logger
source "$HERE"/modules/ZenityPush
source "$HERE"/modules/SimpleConfigManager
source "$HERE"/modules/Locale

function _this_Dialog()
{
    local Selection;
    Selection="$(zenity --list --radiolist --hide-header \
                              --title "$(_Locale_msg main.dialog.title)" --text "$(_Locale_msg main.dialog.description)"\
                              --column "$(_Locale_msg dialog.select)" \
                              --column "" \
                              --column "$(_Locale_msg dialog.an_option)" \
                              --hide-column=2 \
                              true "Install" "$(_Locale_msg main.dialog.select.install_for_user "$USERNAME")" \
                              false "FontPatch" "$(_Locale_msg main.dialog.select.install_font_for_user "$USERNAME")" \
                              false "GdiPlusFix" "$(_Locale_msg main.dialog.select.install_gdiplus_for_user "$USERNAME")" 2>/dev/null;
    )";

    echo "$Selection";
}

function _this_ThrowErrorAndExit()
{
    local type="${1:-"$(_Locale_msg generic.unknown)"}";
    local detail="${2:-"$(_Locale_msg generic.no_info)"}";
    case $type in
        (1 | Network)
            _Logger_Log Error "$(_Locale_msg main.error.network)";
            exit 1;
            ;;
        (2 | SwitchDir)
            _Logger_Log Error "$(_Locale_msg main.error.switch_dir)";
            exit 2;
            ;;
        (3 | InvaildValue)
            _Logger_Log Error "$(_Locale_msg main.error.invaild_value "$detail")";
            exit 3;
            ;;
        (4 | ProgramNotFound)
            _Logger_Log Error "$(_Locale_msg main.error.dependency_not_resolved "$detail")";
            exit 4;
            ;;
        (254 | Debug)
            _Logger_Log Error "$(_Locale_msg main.error.debug)";
            exit 254;
            ;;
        *)
            _Logger_Log Error "$(_Locale_msg main.error.unknown "$type" "$detail")";
            exit 255;
            ;;
    esac;
}

function _this_Download()
{
    local re=0;
	local Source="$1";
    local Title="$2";
    local TargetFile="$3";

	_Logger_Log Info "$(_Locale_msg main.download.info "$Source" "$Title" "$TargetFile")";

    if [ -n "$TargetFile" ] && [ -f "$TargetFile" ] ;then
    {
        _Logger_Log Warn "检测到$TargetFile已存在，将不会重复下载";
    };fi;

	for ((re=1;re<=3;re++));do
    {
        if wget "$Source" -c -O "$TargetFile" 2>&1 \
		        |sed -u "s/^.* \+\([0-9]\+%\) \+\([0-9.]\+[GMKB]\) \+\([0-9hms.]\+\).*$/\1\n# $(_Locale_msg main.download.description "$Title")/"\
		        |zenity --progress --title="$(_Locale_msg main.download.downloading "$Title" "$re")" --auto-close 2>/dev/null;then
        {
            break;
        };else
        {
            if [ "$re" -eq 3 ];then
            {
                _Logger_Log Error "$(_Locale_msg main.download.failed)";
                return 1;
            }
            fi;
        };fi;
    };done;

    return 0;
}

function _this_CheckEnvironment()
{
    local commandline="$1";
    if [ -z "$commandline" ];then
        _this_ThrowErrorAndExit InvaildValue "$(_Locale_msg generic.cannot_be_null "commandline")";
    fi;

    _Logger_Log Info "$(_Locale_msg generic.checking "$commandline")";
    if ! command -v "$commandline" > /dev/null;then
        _this_ThrowErrorAndExit ProgramNotFound "$commandline";
    fi;
}

function _this_InstallGdiPlusToPrefix()
{
    if [ -z "$WINEPREFIX" ];then
        _this_ThrowErrorAndExit InvaildValue "$(_Locale_msg generic.cannot_be_null "WINEPREFIX")";
    fi;

    _this_CheckEnvironment cabextract;

    local _THIS_GDIPLUS_TMPDIR="$HERE/TMP";
    mkdir -p "$_THIS_GDIPLUS_TMPDIR" || _this_ThrowErrorAndExit MakeDir "$(_Locale_msg main.additional.gdiplus.cannot_create_tempdir "$_THIS_GDIPLUS_TMPDIR")";

    _Logger_Log Info "$(_Locale_msg main.additional.gdiplus.download.info)";

    #windows6.1-KB976932-X86.exe
    if _ZenityPush_Question "要使用本地的windows6.1-KB976932-X86.exe吗?"; then
    {
        if ! gdiplus_exe="$(_ZenityPush_FileSelection "请选择windows6.1-KB976932-X86.exe的位置")";then
        {
            _this_ThrowErrorAndExit 4 "无法确定windows6.1-KB976932-X86.exe的位置。"
        };fi
    }
    else
    {
        gdiplus_exe="GdiPlus.exe"
        _this_Download "http://download.windowsupdate.com/msdownload/update/software/svpk/2011/02/windows6.1-kb976932-x86_c3516bc5c9e69fee6d9ac4f981f5b95977a8a2fa.exe" \
            "$(_Locale_msg main.additional.gdiplus.download.description)" "GdiPlus.exe";
    };fi;

    _Logger_Log Info "$(_Locale_msg main.additional.gdiplus.extract_dll)";
    cabextract -q -d "$_THIS_GDIPLUS_TMPDIR" -L -F "x86_microsoft.windows.gdiplus_6595b64144ccf1df_1.1.7601.17514_none_72d18a4386696c80/gdiplus.dll" \
        "$gdiplus_exe";

    cp "$_THIS_GDIPLUS_TMPDIR/x86_microsoft.windows.gdiplus_6595b64144ccf1df_1.1.7601.17514_none_72d18a4386696c80/gdiplus.dll" \
        "$WINEPREFIX/drive_c/windows/system32/gdiplus.dll" || _ZenityPush_Error "$(_Locale_msg main.additional.gdiplus.extract_failed)";
                
    rm -rf "$_THIS_GDIPLUS_TMPDIR";

    wine regedit "$RESOURCES/regedit/patch-dll-overrides.reg";
    _SimpleCM_SetValue "GdiPlus" true;
}

function _this_ParseOptionalOptions()
{
    while [ $# -gt 0 ]; do
    {
        case "$1" in
            (GdiPlus)
                echo "#$(_Locale_msg main.additional_options.gdiplus)";
                _this_InstallGdiPlusToPrefix;
                shift;
                ;;

            (DesktopIcon)
                _Logger_Log Info "DesktopIcon";
                echo "#$(_Locale_msg main.additional_options.desktop_icon)";
				mkdir -vp "$HOME/.local/share/applications/";
			    if ! _this_Download "https://github.com/ppy/osu/raw/master/assets/lazer.png" "$(_Locale_msg main.additional.desktop_icon.name)" "$OSUPREFIX/osu.png";then
			    	_Logger_Log Error "$(_Locale_msg main.additional.desktop_icon.download_failed)"
			    fi
                cat > "$HOME/.local/share/applications/wine-osu.desktop" << EOF
[Desktop Entry]
Encoding=UTF-8
Name=osu!
Comment=$(_Locale_msg main.additional.desktop_icon.file_description)
Type=Application
Exec=env WINEPREFIX=$WINEPREFIX wine osu!.exe
Icon=$OSUPREFIX/osu.png
StartupWMClass=osu!.exe
Categories=
Path=$WINEPREFIX/drive_c/users/$USER/Local Settings/Application Data/osu!
Terminal=false
StartupNotify=false
EOF
                _SimpleCM_SetValue "DesktopIcon" true;
                shift;
                ;;

            (Font)
                _Logger_Log Info "$(_Locale_msg main.additional.font.name) $(_Locale_msg main.additional.font.copyfont)";
                echo "#$(_Locale_msg main.additional.font.patchinfo)";
                cp "$RESOURCES/font/fake-msyh.ttf" "$WINEPREFIX/drive_c/windows/Fonts";
                cp "$RESOURCES/font/malgun.ttf" "$WINEPREFIX/drive_c/windows/Fonts";

                _Logger_Log Info "$(_Locale_msg main.additional.font.name) winetricks Tahoma";
                winetricks tahoma;

                _Logger_Log Info "$(_Locale_msg main.additional.font.name) $(_Locale_msg main.additional.font.regpatch)";
                wine regedit "$RESOURCES/regedit/patch-Font.reg";
                _Logger_Log Warn "$(_Locale_msg main.additional.font.warning)";

                _SimpleCM_SetValue "Font" true;
                shift;
                ;;

            (Language)
                _Logger_Log Info "$(_Locale_msg main.additional.setup_language.progress)";
                echo "#$(_Locale_msg main.additional.setup_language.progress)";
                if [ -z "$OSUPREFIX" ];then
                    _Logger_Log Error "$(_Locale_msg generic.error.osu.cannot_confirm_container)";
                fi;

                if [ "$DownloadSource" != "sayobot" ];then
                    _Logger_Log Warn "$(_Locale_msg main.additional.setup_language.warning)";
                fi;

                cat > "$OSUPREFIX/osu!.$USERNAME.cfg" << EOF
# 【重要】请勿公开分享此文件！
# 该文件中包含登录帐号密码信息！
ChatChannels = #osu #chinese
ChatLastChannel = #chinese
Language = zh-CHS
EOF
                _SimpleCM_SetValue "Language" true;
                shift;
                ;;

            (*)
                _Logger_Log Error "$(_Locale_msg main.error.invaild_value "$1")";
                shift;;
        esac;
    };done;
}

function _this_RunInstall()
{
    #region 一些基础的东西
    local InstallerFile="";
    local DownloadSource="";
    #endregion

    #region 准备安装

    #初始化wine的环境变量
    if ! WinePrefix="$(_ZenityPush_DirectorySelection "$(_Locale_msg generic.select.osu.container)")";then
    {
        _this_ThrowErrorAndExit InvaildValue "$(_Locale_msg generic.error.osu.cannot_confirm_container)";
    };fi;

    if ! OSUPREFIX="$(_ZenityPush_DirectorySelection "$(_Locale_msg generic.select.osu.gamedir)")";then
    {
        _this_ThrowErrorAndExit InvaildValue "$(_Locale_msg generic.error.osu.cannot_confirm_gamedir)";
    };fi;

    export WINEPREFIX="$WinePrefix";
    export WINEARCH=win32;

    #region 下载osu!installer
    _Logger_Log Info "$(_Locale_msg main.runinstall.download_program)";
    echo "#$(_Locale_msg main.runinstall.download_program)"

    DownloadSource="$(zenity --list --radiolist --hide-header \
                              --title "$(_Locale_msg main.runinstall.downloadsource.select.title)" --text "$(_Locale_msg main.runinstall.downloadsource.select.description)"\
                              --column "$(_Locale_msg dialog.select)" \
                              --column "" \
                              --column "$(_Locale_msg dialog.an_option)" \
                              --hide-column=2 \
                              true "ppy" "$(_Locale_msg main.runinstall.downloadsource.select.ppy)" \
                              false "sayobot" "$(_Locale_msg main.runinstall.downloadsource.select.sayobot)" \
                              false "none" "$(_Locale_msg main.runinstall.downloadsource.select.none)" 2>/dev/null;
    )";

    case "$DownloadSource" in
        ppy)
            if _this_Download "https://m1.ppy.sh/r/osu!install.exe" "osu!installer" "osu_install.exe";then
                InstallerFile="osu_install.exe";
            else
                _ZenityPush_Error "$(_Locale_msg main.runinstall.installer_download_failed)";
            fi;
            ;;
        sayobot)
            _this_InstallFromSayobot;
            ;;
        none)
            _Logger_Log Info "$(_Locale_msg main.runinstall.will_configure_container_only)";
            ;;
    esac

    #endregion 下载osu!installer

    #endregion 准备安装

    echo "# ";
    _ZenityPush_Info "$(_Locale_msg main.runinstall.prepare_complete)";

    #region 初始化wine容器

    #初始化配置文件
    echo "#$(_Locale_msg main.runinstall.setup.config)"
    ContainerName="$(echo $RANDOM | md5sum |cut -c 1-8)";
    _SimpleCM_ChangeTargetFile "_osu-stable-$ContainerName.ini";

    _SimpleCM_SetValue "WinePrefix" "$WINEPREFIX";
    _SimpleCM_SetValue "OsuGameDirectory" "$OSUPREFIX";
    _SimpleCM_SetValue "ContainerName" "$ContainerName";
    _SimpleCM_SetValue "DownloadSource" "$DownloadSource";
    _SimpleCM_SetValue "InstallTime" "$(date +%Y-%m-%d-%T)";
    
    _SimpleCM_SetValue "Install-GdiPlus" false;
    _SimpleCM_SetValue "Install-DesktopIcon" false;
    _SimpleCM_SetValue "Install-Font" false;
    _SimpleCM_SetValue "Install-Language" false;

    mkdir -vp "$OSUPREFIX";
	mkdir -vp "$WINEPREFIX/drive_c/users/$USERNAME/Local Settings/Application Data";

    #软链
	ln "$OSUPREFIX" "$WINEPREFIX/drive_c/users/$USERNAME/Local Settings/Application Data/osu!" -s;

    #创建wine容器
    echo "#$(_Locale_msg main.runinstall.setup.create_wine_container)";
    wineboot -i;
    #endregion 初始化wine容器

    #region 预先处理
    local OptionalOptions;
    OptionalOptions="$(zenity --list --checklist --separator=" " --hide-header \
                              --title "$(_Locale_msg main.additional_options.title)" --text "$(_Locale_msg main.additional_options.description)" \
                              --column "$(_Locale_msg dialog.select)" --column "" --column "$(_Locale_msg dialog.an_option)" \
                              --hide-column=2 \
                              TRUE "GdiPlus" "$(_Locale_msg main.additional_options.gdiplus)" \
		                      TRUE "DesktopIcon" "$(_Locale_msg main.additional_options.desktop_icon)" \
		                      TRUE "Font" "$(_Locale_msg main.additional_options.font)" \
                              TRUE "Language" "$(_Locale_msg main.additional_options.setup_language)" 2>/dev/null
    )";
    _this_ParseOptionalOptions $OptionalOptions;
    #endregion 预先处理

    #启动安装程序
    echo "# 安装.NET"
    if _ZenityPush_Question "要使用winetricks安装.NET吗?"; then
    {
        _Logger_Log Info "$(_Locale_msg main.runinstall.progress.start_msg)";
        echo "#$(_Locale_msg main.runinstall.progress.dotnet40.title)";
        _Logger_Log Warn "$(_Locale_msg main.runinstall.progress.dotnet40.warning)";
        winetricks dotnet40;
    }
    else
    {
        _Logger_Log Info "从本地程序安装.NET 4.0";
        if ! DOTNET40INSTALLER="$(_ZenityPush_FileSelection "$(_Locale_msg generic.select.dotnet40.installer)")";then
        {
            _this_ThrowErrorAndExit 4 "$(_Local_msg generic.select.dotnet40.installerNotFound)";
        };fi

        wine "$DOTNET40INSTALLER"
    };fi;

    _Logger_Log Info "$(_Locale_msg main.runinstall.progress.dll_patch)";
    echo "#$(_Locale_msg main.runinstall.progress.dll_patch)";
    wine regedit "$RESOURCES/regedit/patch-dll-overrides.reg";

    if [ "$DownloadSource" == "ppy" ];then
    {
        echo "#$(_Locale_msg main.runinstall.progress.run_osu_installer)";
        wine "$InstallerFile";
        echo "#$(_Locale_msg main.runinstall.progress.wait_for_wineserver)";
        wineserver -w;
    };fi;

    echo "#$(_Locale_msg main.runinstall.progress.set_windows_version)"
    winetricks winxp;

    _ZenityPush_Info "$(_Locale_msg main.runinstall.done "_osu-stable-$ContainerName.ini")";
    echo 100;
}

function _this_InstallFromSayobot()
{
    echo "#$(_Locale_msg generic.check_dependencies)";
    _this_CheckEnvironment "unzip";

    if [ -z "$OSUPREFIX" ];then
        _this_ThrowErrorAndExit InvaildValue "$(_Locale_msg generic.cannot_be_null "OSUPREFIX")";
    fi;

    echo "#$(_Locale_msg main.sayobotinstall.progress.download)";
    _Logger_Log Info "$(_Locale_msg main.sayobotinstall.donate1)"; #非恰饭，是真的良心
    _Logger_Log Info "$(_Locale_msg main.sayobotinstall.donate2)";
    if ! _this_Download "https://dl.sayobot.cn/osu.zip" "$(_Locale_msg main.sayobotinstall.download.pack.title)" "osu.zip";then
    {
        _Logger_Log Error "$(_Locale_msg main.sayobotinstall.download.pack.failed)";
        _ZenityPush_Error "$(_Locale_msg main.sayobotinstall.download.pack.failed)";
        _this_ThrowErrorAndExit Network;
    }
    fi;

    #教程地图不是强制性的，因此下载失败或取消不会影响任何进度
    _this_Download "https://dl.sayobot.cn/beatmaps/download/full/1011011" "$(_Locale_msg main.sayobotinstall.download.tutorial.title)" "1011011 nekodex - new beginnings.osz"

    echo "#$(_Locale_msg main.sayobotinstall.extract "$OSUPREFIX")";
    if ! unzip osu.zip -d "$OSUPREFIX";then
    {
        _this_ThrowErrorAndExit UnzipFailed "$(_Locale_msg main.sayobotinstall.extract.error)";
    };fi;
    mv "1011011 nekodex - new beginnings.osz" "$OSUPREFIX/Songs";

    echo "#$(_Locale_msg generic.done)";
}

function _this_Main()
{
    #region 检查环境
    _Logger_Log Info "$(_Locale_msg generic.check_dependencies)";
    _this_CheckEnvironment zenity;
    _this_CheckEnvironment wine;
    _this_CheckEnvironment winetricks;

    #检查wine版本是否会遇到问题
    if [ -n "$(wine --version | grep -- "-5.")" ];then
    {
        _Logger_Log Warn "$(_Locale_msg main.wine5.warn)";
    };fi;
    #endregion

    case "$(_this_Dialog)" in
        (Install)
            _this_RunInstall | zenity --progress --pulsate --text="$(_Locale_msg main.zenity_progress.placeholder)" --auto-close --no-cancel 2>/dev/null;
            ;;
        (FontPatch)
            local main_WinePrefix;
            if ! _SimpleCM_ChangeTargetFile "$(_ZenityPush_FileSelection "$(_Locale_msg misc.select.config)")";then
                _this_ThrowErrorAndExit InvaildValue "$(_Locale_msg generic.error.osu.cannot_confirm_container)";
            fi;
            main_WinePrefix="$(_SimpleCM_GetValue "WinePrefix")";

            if ! _ZenityPush_Question "$(_Locale_msg main.fontpatch.question.text "$main_WinePrefix")";then
            {
                _Logger_Log Info "$(_Locale_msg generic.canceled)";
                exit 0;
            };fi;

            export WINEPREFIX="$main_WinePrefix";
            _this_ParseOptionalOptions Font;
            exit 0;
            ;;

        (GdiPlusFix)
            local main_WinePrefix;
            if ! _SimpleCM_ChangeTargetFile "$(_ZenityPush_FileSelection "$(_Locale_msg misc.select.config)")";then
                _this_ThrowErrorAndExit InvaildValue "$(_Locale_msg generic.error.osu.cannot_confirm_container)";
            fi;
            main_WinePrefix="$(_SimpleCM_GetValue "WinePrefix")";

            _Logger_Log Info "$(_Locale_msg main.gdiplusfix.info "$USERNAME" "$main_WinePrefix")";
            if _ZenityPush_Question "$(_Locale_msg main.gdiplusfix.info "$USERNAME" "$main_WinePrefix")";then
                export WINEPREFIX="$main_WinePrefix";
                _this_InstallGdiPlusToPrefix;
            else
                _this_ThrowErrorAndExit InvaildValue "$(_Locale_msg generic.canceled)";
            fi;
            ;;
        (*)
            _this_ThrowErrorAndExit InvaildValue "$(_Locale_msg generic.canceled)";
            ;;
    esac;
}

_this_Main;
