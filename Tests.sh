#!/bin/bash
#########################
#   名称: Tests
#   用途: 测试各模块是否按照预定的进行
#########################

#shellcheck disable=SC1090
#shellcheck disable=SC2143

readonly HERE="$( dirname "$(readlink -f "${0}")" )";
readonly RESOURCES="$HERE/resources";

source "$RESOURCES"/script/Statics
source "$RESOURCES"/script/Logger
source "$RESOURCES"/script/ZenityPush

#region Logger
printf "\n==========\n测试内容: _Logger_Log\n==========\n\n";
function _this_TestLoggerLog()
{
    local Level="$1";
    _Logger_Log "$Level" "这是一个$Level信息";
}
_this_TestLoggerLog Info;
_this_TestLoggerLog Warn;
_this_TestLoggerLog Warning;
_this_TestLoggerLog Error;
_this_TestLoggerLog Ok;
_this_TestLoggerLog Unknown;
_this_TestLoggerLog Custom;
#endregion Logger

#region Zenity
printf "\n==========\n测试内容: ZenityPush\n==========\n\n";

_Logger_Log Info "测试基本的ZenityPush";

_ZenityPush_Info;
_Logger_Log Info "返回值为$?";

_ZenityPush_Error;
_Logger_Log Info "返回值为$?";

_ZenityPush_Warn;
_Logger_Log Info "返回值为$?";

ret="";
_ZenityPush_Question;
ret="$?";
if [ "$ret" -eq 0 ];then
    _Logger_Log Info "选择了\"是\"";
elif [ "$ret" -eq 1 ];then
    _Logger_Log Info "选择了\"否\"";
else
    _Logger_Log Error "未知的值$ret";
fi;
unset ret;

_Logger_Log Info "测试文件选择：无mime要求";
path="";
path="$(_ZenityPush_FileSelection)";
_Logger_Log Info "选择了 \"$path\"";
unset path;

_Logger_Log Info "测试文件选择：mime要求为text/plain";
path="";
path="$(_ZenityPush_FileSelection "" "text/plain")";
_Logger_Log Info "选择了 \"$path\"";

_Logger_Log Info "测试目录选择";
path="";
path="$(_ZenityPush_DirectorySelection)";
_Logger_Log Info "选择了 \"$path\"";