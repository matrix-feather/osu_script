# osu_script

## Note
**安装前你可能需要检查当前显卡驱动的32位版本是否已经安装。**

## 介绍
一个脚本搞定osu!stable在linux上的安装

## 依赖
* wine(最基本的依赖，用来运行osu!stable)
* winetricks(安装GDI+等)
* zenity(创建对话框)
* unzip(如果要从sayobot下载)
* cabextract(如果要安装GDI+)

## 项目引用
* fake-msyh -> [文泉驿微米黑](https://sourceforge.net/projects/wqy/)

## 安装教程

### [视频](https://www.bilibili.com/video/BV19K4y1v78Q)

### 懒人式

复制以下指令，粘贴到终端中(需要预先安装git)\
`git clone https://gitee.com/matrix-feather/osu_script.git && cd osu_script && chmod +x ./Install.sh && ./Install.sh`

### 详细步骤

1. 克隆仓库
2. 前往仓库目录
3. 给予`Install.sh`运行权限
4. 在终端中运行`Install.sh`

## 测试平台
- [x] Ubuntu 20.10
- [x] Deepin V20

## 效果图

![主界面设置菜单](https://s1.ax1x.com/2020/08/12/avbKQf.png)
![选歌界面](https://s1.ax1x.com/2020/08/12/avbQOS.png)
![编辑器](https://s1.ax1x.com/2020/08/12/avbuSP.png)
![多人游戏大厅](https://s1.ax1x.com/2020/08/12/avbMy8.png)

## 使用说明

1.  在Deepin V20上您可能需要使用deepin-wine5运行osu!以避免编辑器顶栏黑条问题 
2.  GDI+可能会因为网络问题而安装失败
3.  ~~目前还没有找到能解决韩文堆在一起的方法~~(已通过安装[malgun](https://zh.wikipedia.org/zh-hans/Malgun_Gothic)字体解决)

## TODO
- [x] 修复韩文堆在一起的问题
- [x] 解决"▴"和"▾"不显示的问题
- [x] 解决"〜"不显示的问题
- [ ] 或许可以试试用`Noto Sans CJK`来代替`WenQuanYi Micro Hei`
- [ ] 解决泰文不显示, 显示为方框的问题 (*需要帮助: 找不到字体, 网上查不到, 用Tahoma也不显示*)
- ~~[ ] 解决字体偏高的问题 (*需要帮助*)~~(原版微软雅黑也有这问题)

## 参与贡献

1.  Fork 本仓库
2.  新建一个分支
3.  解决TODO中未解决的问题、解决一个未被发现的问题或提交一个新的功能
4.  新建 Pull Request
